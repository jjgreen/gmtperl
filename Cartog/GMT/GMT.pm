package Cartog::GMT;

# an interface to the generic mapping tools (GMT)
# (c) J.J. Green 2001, 2002

# this is very outdated, but still works after a fashion

use 5.006;
use strict;
use warnings;

require Exporter;
our @ISA = qw(Exporter);

our %EXPORT_TAGS = ( 'all' => [ qw() ] );
our @EXPORT_OK = ( @{ $EXPORT_TAGS{'all'} } );
our @EXPORT = qw();
our $VERSION = '0.85';

1;

package Layer;

use Class::Struct;
use IPC::Open2;

struct(
       program => '$',
       options => '$',
       input   => '$',
       data    => '$',
       wrapper => '$',
       );

sub set
{
    my $self = shift;

    $self->program(shift);
    $self->options(shift);
    $self->input(shift);
    $self->data(shift);
    $self->wrapper(shift);
}

sub addoption
{
    my $self = shift;
    my ($extra) = @_;

    my $options = $self->options || "";
    $self->options("$options $extra");
}

sub draw
{
    my $self = shift;
    my ($strmout) = @_;

    my $wrapper  = $self->wrapper;
    my $program  = ($wrapper ? "$wrapper " : "") . $self->program;
    my $options  = $self->options;
    my $input    = $self->input;

    if (ref $input)
    {
	# $input is a reference and so assumed to be
	# a reference to a function f(*STREAM,$data),
	# which writes to STREAM and may use the optional
	# argument $data, and return 1 on success

	my $callback = $input;
	my $data     = $self->data;

	# this is a bit tricky
	# we open2() to read & write to the comman

	my ($read, $write);
	my $cmd = "$program $options";

	my $opid = open2($read, $write, $cmd);

	# now fork to run reading writing simultaneously

	my $fpid = fork;

	if ($fpid < 0)
	{
	    warn "fork failed for command $cmd\n";
	    return 0;
	}

	if ($fpid)
	{
	    # parent

	    close $read;

	    # lets the callback write to the process

	    unless ($callback->($write, $data))
	    {
		warn "callback returned error\n";
		return 0;
	    }

	    close $write;

	    # reap child process

	    waitpid $fpid, 0;
	}
	else
	{
	    # child

	    close $write;

	    # reads from the process out and writes the
	    # result to out output stream *except* the EOF
	    # (we dont want to close the output stream)

	    while (my $line = <$read>)
	    {
		print $strmout $line;
	    }

	    close $read;

	    exit 0;
	}

	# harvest the open2 child

	waitpid $opid, 0;
    }
    else
    {
	# if $input is defined (but not a reference) then is assumed
	# to be a system command to execute and whose output is piped
	# to the GMT program. this is much easier

	my $cmd = ($input ? "$input | " : "") . "$program $options";

	# popen the process

	unless (open PROC, "$cmd |")
	{
	    warn "failure opening process \"$cmd\"\n";
	    return 0;
	}

	# write the process' output to the output filehandle
	# (array context means all the lines are written)

	unless (print $strmout <PROC>)
	{
	    warn "failure writing the output of \"$cmd\"\n";
	    return 0;
	}

	unless (close PROC)
	{
	    warn "failure closing process \"$cmd\" (exit $?)\n";
	    return 0;
	}
    }

    return 1;
  }

1;

package Layers;

use Class::Struct;

sub new { return bless []; }

sub append
  {
    my $self = shift;
    my $layer = new Layer;

    $layer->set(@_);

    push @$self, $layer;
  }

sub AUTOLOAD
  {
    my $self = shift;
    my $program = $Layers::AUTOLOAD;

    $program =~ s/.*:://;

    $self->append($program, @_);
  }

1;

package Map;

use Class::Struct;
use FileHandle;

struct(
       output   => '$',
       defaults => '$',
       layers   => 'Layers',
       verbose  => '$',
       wrapper  => '$',
      );

# draw the layers

sub draw
  {
    my $self = shift;

    # for less code-noise

    my $defaults = $self->defaults;
    my $layers   = $self->layers;
    my $wrapper  = $self->wrapper;

    if ($defaults)
      {
	if (not -e $defaults)
	  {
	    warn "defaults file $defaults not found\n";
	    return 0;
	  }
      }

    # open the output (postscript) file

    my $output = $self->output;
    my $stream;

    if ($output)
    {
	$stream = new FileHandle;
	if (not $stream)
	{
	    warn "failed to create filehandle\n";
	    return 0;
	}
	if (not $stream->open("> $output"))
	{
	    warn "failed to open file $output\n";
	    return 0;
	}
    }
    else
    {
	$stream = *STDOUT;
    }

    # add -K (to all but the last layer) and -O
    # to all but the first)

    my $excluded;

    $excluded = shift @$layers;
    foreach my $layer (@$layers){ $layer->addoption("-O"); }
    unshift @$layers, $excluded;

    $excluded = pop @$layers;
    foreach my $layer (@$layers){ $layer->addoption("-K"); }
    push @$layers, $excluded;

    # draw each layer

    my $lcount = 0;

    foreach my $layer (@$layers)
    {
	$layer->wrapper($wrapper) if $wrapper;
	$layer->addoption("+$defaults") if $defaults;

	if ($layer->draw($stream))
	{
	    $lcount++;
	    printf "  %s\n", $layer->program
		if $self->verbose;
	}
	else
	{
	    unlink $output;
	    warn "output file \"$output\" not created\n";
	    return 0;
	}
    }

    printf "%i layer%s written to %s\n", $lcount, plural($lcount), $output
	if $self->verbose;

    return $lcount;
  }

# pedantry!

sub plural
  {
    my $num = shift;
    return ($num==1 ? "" : "s");
  }

1;

__END__
=head1 NAME

Cartog::GMT - Perl extension for the Generic Mapping Tools (GMT)
cartographic package.

=head1 SYNOPSIS

  use Cartog::GMT;

  # set up the map

  $map = new Map;

  $map->output('test.eps');
  $map->defaults('defaults.txt');
  $map->verbose(1);

  # get a layers set

  $layers = new Layers;

  # Each GMT program has an associated layer method of the same name.

  $layers->pscoast("$common $res -Sc");

  # a second argument which is a string is executed with its output
  # piped to the GMT program.

  $layers->grdimage("$common -C$ccpt", "cat topo.grd");
  $layers->grdcontour("$common -C5 -A20f6 -S20 -W2/200", "cat topo.grd");

  # give the map its layers set

  $map->layers($layers);

  # draw it

  $map->draw() or die;

=head1 DESCRIPTION

The Generic Mapping Tools (GMT) is a collection of
programs for cartography, and more general technical
illustration, with output in high-quality PostScript.
This module provides a simple interface to these tools.

=head2 Map class

The package offers a I<Map> class, with instantiation
in the usual perl manner:

  $map = new Map;

Map I<methods> allow one to specify a (PostScript) output
file, a file of GMT defaults values (see gmtset(1)),
a wrapper script (see below) and the verbosity level.

  $map->defaults('defaults.txt');
  $map->output('test.eps');
  $map->wrapper('GMT')
  $map->verbose(1);

The map's layers method requires a I<Layers> object.

  $map->layers($layers);

=head2 Layers class

The Layers object implements a list of system calls to
the GMT programs.  One must must initialise the object

  $layers = new Layers;

before passing it to the map, or use

  $layers = $map->layers(new Layers);

to initialise and assign at the same time.

Each GMT program has an associated Layers method of the
same name. The first argument of the method will be used
as the system call arguments for the program.

  $layers->pscoast("$common $res -Sc");

If the second argument is present then its type determines
the iterpretation: a string is interpreted as a system command
whose output is piped to the GMT program; the following are
equivalent.

  $layers->grdimage("topo.grd $common -C$ccpt");
  $layers->grdimage("$common -C$ccpt", "cat topo.grd");

If the second argument is a (reference to) a function
then it is assumed to take the form

  callback(*STREAM, $data);

and should write, to the STREAM, lines of input for the
GMT command. The optional second argument can be used to
pass data to the callback function without introducing global
variables.  The following code

  sub mycat
  {
    my ($stream, $file) = @_;
    open FILE, "< $file";
    print $stream <FILE>;
    close FILE;
    return 1;
  }

  $layers->pstext("$common -D0/0p", \&mycat, "villages.dat");

is functionally equivalent to

  $layers->pstext("villages.dat $common -D0/0p");

ie, it feeds the file villages.dat to pstext, but with the
callback method one has the opportunity to filter the contents
of a file before pstext reads it. This functionality is experimental;
it uses open2() and fork() to route the data and so may be subject
to deadlock and program hangs. Use with caution.

Note that the -K and -O options (specifying whether each
layer is an initial or final layer) are generated
automatically by the Cartog::GMT module, and need not
be included in the options.

=head2 Drawing

When the layers have been specified, the map may be drawn
with the Map draw method:

  $map->draw() or die;

Note that the GMT commands are not executed until the
draw method has been called, and that they are then
executed in the order in which the layer methods were
called. This incudes the execution of the callback
functions mentioned above.

=head2 Wrappers

In some situations it is convenient to call a wrapper
script to set the environment before calling GMT programs.
For example, the Debian Linux system does not put the
GMT programs on the usual path, and one needs to call
the wrapper script 'GMT' to access the programs:

  GMT pscoast ...

and so on. To use a wrapper 'GMT' use the wrapper method
on your Map.

  $map->wrapper('GMT');

=head2 Caveats

The layer methods are automatically generated with
AUTOLOAD, so that a Layers method call such as

  $layers->pscroast("$common $res -Sc");

will not produce an error until the failure of the
corresponding system call following $map->draw().

Note that layer methods should only be (the names of)
GMT programs which produce PostScript output. No checking
that this is the case is done.

=head2 EXPORT

None by default.

=head1 AUTHOR

J.J. Green

=head1 SEE ALSO

GMT(1),
perl(1).

=cut
