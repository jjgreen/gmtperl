PMDIR = Cartog/GMT
DIST  = gmtperl.tar.gz

$(DIST) : gmtperl-clean
	tar -zcvf $(DIST) --exclude-from=dist-exclude $(PMDIR)

gmtperl-clean :
	cd $(PMDIR) ; perl Makefile.PL ; $(MAKE) clean

test :
	cd $(PMDIR) ; perl Makefile.PL ; $(MAKE) test

clean : gmtperl-clean

